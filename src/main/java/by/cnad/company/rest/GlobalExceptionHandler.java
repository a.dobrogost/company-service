package by.cnad.company.rest;

import by.cnad.company.service.ResourceNotFoundException;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler({ResourceNotFoundException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ErrorResponse processNotFoundError(ResourceNotFoundException ex) {
    return ErrorResponse.builder()
        .code("123")
        .message(ex.getMessage())
        .build();
  }

  @Data
  @Builder
  private static class ErrorResponse {
    private String code;
    private String message;
  }

}
