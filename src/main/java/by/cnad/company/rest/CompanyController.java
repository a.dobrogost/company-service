package by.cnad.company.rest;

import by.cnad.company.service.CompanyDto;
import by.cnad.company.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/companies")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CompanyController {

  private final CompanyService service;

  @Value("${supplier.version}")
  private String version;

  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String sayHello() {
    return "Hello from supplier service version: " + version;
  }


  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public CompanyDto getCompany(@PathVariable Long id) {
    return service.getCompanyById(id);
  }



}
